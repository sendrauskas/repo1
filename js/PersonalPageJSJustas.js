 document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.dropdown-trigger');
    var instances = M.Dropdown.init(elems);
  });

   document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.carousel');
    var instances = M.Carousel.init(elems);
  });
   document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems);
  });

google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {										//pirmas grafiko pvz
  var data = google.visualization.arrayToDataTable([
  ['Task', 'Valandos per dieną'],
  ['Darbas', 8],
  ['Draugai',1],
  ['Skanus maistas', 1],
  ['TV', 0],
  ['Gym', 0],
  ['Miegas', 6]
]);

  var options = {'title':'Mano įprasta diena', 'width':550, 'height':400};
  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart')); //---->> cia galima kitu grafiku pasirinkti :https://developers.google.com/chart/interactive/docs/gallery/
  chart.draw(data, options);
}




var ctx = document.getElementById('myChart');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ['Pirmadienis', 'Antradienis', 'Trečiadienis', 'Ketviratdienis', 'Penktadienis', 'Šeštadienis','Sekmadienis'],
        datasets: [{
            label: 'Pinigų kiekis išleistas per savaitę EUR',
            data: [10, 6, 2, 5, 8, 9, 5],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 2
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

const PIE = document.getElementById("polarChart");
 console.log(PIE);
 let polarChart = new Chart(PIE, {
  type: 'polarArea',
  data: {
  labels: ['Jėga', 'Sveikata', 'Greitis', 'Sekmė'], //rasom ka norim 

  datasets: [{
  label: 'Pinigų kiekis išleistas per savaitę EUR',
  backgroundColor: ['#f1c40f','#e67e22', '#16a085' ,'#2980b9'], //spalvos
  data: [30, 60, 35, 30] // duomenys
 }]
},
options:{
  animation:{
    animateScale: false 

    
  }
}
});