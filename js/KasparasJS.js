 document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.dropdown-trigger');
    var instances = M.Dropdown.init(elems);
  });

   document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.carousel');
    var instances = M.Carousel.init(elems);
  });
   document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems);
  });








    google.charts.load('current', {
        'packages':['geochart'],
        // Note: you will need to get a mapsApiKey for your project.
        // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
        'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY'
      });
      google.charts.setOnLoadCallback(drawRegionsMap);

      function drawRegionsMap() {
        var data = google.visualization.arrayToDataTable([
          ['Country', 'No'],
          ['Austria', 23],
          ['Belgium', 10],
          ['Bosnia and Herzegovina', 13],
          ['Croatia', 12],
          ['Cuba', 33],
          ['Chech Republic', 25],
          ['Denmark', 5],
          ['Egypt', 7],
          ['Estonia', 19],
          ['Finland', 20],
          ['France', 26],
          ['Germany', 4],
          ['Hungary', 1],
          ['Israel', 8],
          ['Italy', 27],
          ['Latvia', 2],
          ['Liechtenstein', 22],
          ['Lithuania', 1],
          ['Monaco', 28],
          ['Montenegro', 14],
          ['Netherlands', 11],
          ['Norway', 31],
          ['Poland', 3],
          ['Portugal', 30],
          ['Singapore', 34],
          ['Slovakia', 6],
          ['Slovenia', 24],
          ['Spain', 29],
          ['Sweden', 16],
          ['Switzernald', 17],
          ['Turkey', 15],
          ['Ukraine', 32],
          ['United Kingdom', 18],
          ['United States', 21],
          ['Vietnam', 35],

        ]);

        var options = {};

        var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));

        chart.draw(data, options);
      }

      function download(){


      };