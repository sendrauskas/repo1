 document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.dropdown-trigger');
    var instances = M.Dropdown.init(elems);
  });
  document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.carousel');
    var instances = M.Carousel.init(elems);
  });

 document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.slider');
    var instances = M.Slider.init(elems);
  });

 const CHART = document.getElementById("lineChart");
 console.log(CHART);
 let lineChart = new Chart(CHART, {
 	type: 'line',
 	data: {
        labels: ['Pirmadienis', 'Antradienis', 'Treciadienis', 'Ketvirtadienis', 'Penktadienis', 'Sestadienis', 'Sekmadienis'],
        datasets: [{
        	//fill: 'true',
            label: 'Isleistu pinigu skaicius per savaite EUR',
            data: [10, 5, 20, 8, 10, 9, 15],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
 });

//  Antras chart 
 const PIE = document.getElementById("pieChart");
 console.log(PIE);
 let pieChart = new Chart(PIE, {
 	type: 'pie',
 	data: {
 	labels: ['jega', 'sveikata', 'greitis', 'sekme'], //rasom ka norim 

 	datasets: [{
 	label: 'points',
 	backgroundColor: ['#f1c40f','#e67e22', '#16a085' ,'#2980b9'], //spalvos
 	data: [10, 20, 55, 30] // duomenys
 }]
}
});

